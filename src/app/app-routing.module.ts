import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'fumetti',
    loadChildren: () => import('./fumetti/fumetti.module').then( m => m.FumettiPageModule)
  },
  {
    path: 'informazioni-base',
    loadChildren: () => import('./informazioni-base/informazioni-base.module').then( m => m.InformazioniBasePageModule)
  },
  {
    path: 'action',
    loadChildren: () => import('./action/action.module').then( m => m.ActionPageModule)
  },
  {
    path: 'videogame',
    loadChildren: () => import('./videogame/videogame.module').then( m => m.VideogamePageModule)
  },
  {
    path: 'giochi-tavolo',
    loadChildren: () => import('./giochi-tavolo/giochi-tavolo.module').then( m => m.GiochiTavoloPageModule)
  },
  {
    path: 'cosplay',
    loadChildren: () => import('./cosplay/cosplay.module').then( m => m.CosplayPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
