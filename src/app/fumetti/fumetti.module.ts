import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FumettiPageRoutingModule } from './fumetti-routing.module';

import { FumettiPage } from './fumetti.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FumettiPageRoutingModule
  ],
  declarations: [FumettiPage]
})
export class FumettiPageModule {}
