import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FumettiPage } from './fumetti.page';

const routes: Routes = [
  {
    path: '',
    component: FumettiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FumettiPageRoutingModule {}
