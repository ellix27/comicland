export class Figure {
    nome : string;  //nome 
    anno : Date;    //data
    marca : string; //costruttore
    linea : string; //
    stato : number; //valutazione da 1 a 10
    prezzo : number;
    
    constructor (nome : string, anno: Date, marca : string, linea: string, stato: number, prezzo : number) {
        this.nome = nome;
        this.anno = anno;
        this.marca = marca;
        this.linea = linea;
        this.stato = stato;
        this.prezzo = prezzo;
    }
    
    getNome() {
        return this.nome;
    }

    getAnno() {
        return this.anno;
    }

    getMarca() {
        return this.marca;
    }

    getLinea() {
        return this.linea;
    }

    getStato() {
        return this.stato;
    }

    getPrezzo() {
        return this.prezzo;
    }

    toString() {
        return this.nome + this.anno.toDateString() + this.marca + this.linea + this.stato.toString() + this.prezzo.toString();
    }
}