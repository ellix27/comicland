export class ViedoGame {
    titolo : string;        // titolo
    condizione : string;    // condizione
    funz : boolean;         // funzionante?
    piatta : string;  
    prezzo: number;
    
    constructor(titolo:string, cond : string, funz : boolean, piatta : string, prezzo : number) {
        this.titolo = titolo;
        this.condizione = cond;
        this.funz = funz;
        this.piatta = piatta;
        this.prezzo = prezzo;
    }

    getTitolo() {
        return this.titolo;
    }

    getCondizione() {
        return this.condizione;
    }

    isWorking() {
        return this.funz;
    }

    getPiattaforma () {
        return this.piatta;
    }
    
    getPrezzo (){
        return this.prezzo;
    }

    toString() {
        return this.titolo + this.condizione + this.funz + this.piatta;
    }
}