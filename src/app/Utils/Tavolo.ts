import * as internal from "stream";

export class Tavolo {
    nome : string;  //nome 
    anno : Date;    //data
    produttore : string; //costruttore
    pezziMancanti : string; //pezzi mancanti
    prezzo: number;

    constructor (nome : string, anno : Date, produttore : string, pezzi : string, prezzo : number){
        this.nome = nome;
        this.anno = anno;
        this.produttore = produttore;
        this.pezziMancanti = pezzi;
        this.prezzo = prezzo;
    }
    
    getNome() {
        return this.nome;
    }

    getAnno() {
        return this.anno;
    }

    getProduttore() {
        return this.produttore;
    }

    getPezziMancanti(){
        return this.pezziMancanti;
    }

    getPrezzo (){
        return this.prezzo;
    }
    
    toString (){
        return this.nome + this.anno.toUTCString() + this.produttore + this.pezziMancanti + this.prezzo;
    }
}