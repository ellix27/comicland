export class Fumetto {
    titolo : string;  // titolo
    anno : Date;      // anno
    descr: string;    // descrizione articolo
    ediz: string;     // edizione 1-2-3-limitata
    val: number;      // valutazione da 1 a 10 in base alle condizioni
    prezzo : number;

    constructor(tit:string, anno:Date, descr:string, ediz:string, val:number, prezzo : number){
        this.titolo = tit;
        this.anno = anno;
        this.descr = descr;
        this.ediz = ediz;
        this.val = val;   
        this.prezzo = prezzo;    
    }

    getName() {
        return this.titolo;
    }

    getAnno() {
        return this.anno;
    }
    
    getDescr() {
        return this.descr;
    }

    getEdiz() {
        return this.ediz;
    }

    getVal() {
        return this.val;
    }

    getPrezzo (){
        return this.prezzo;
    }    

    toString() {
        return this.titolo + this.anno.toDateString() + this.descr + this.ediz + this.val.toString() + this.prezzo.toString();
    }
}