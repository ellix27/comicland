import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-informazioni-base',
  templateUrl: './informazioni-base.page.html',
  styleUrls: ['./informazioni-base.page.scss'],
})
export class InformazioniBasePage implements OnInit {

  constructor(private route: Router) { }

  ngOnInit() {
  }

  goAnOtherPage(){
    this.route.navigate["/fumetti"];
  }

}
