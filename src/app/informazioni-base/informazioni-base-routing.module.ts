import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FumettiPage } from '../fumetti/fumetti.page';
import { InformazioniBasePage } from './informazioni-base.page';

const routes: Routes = [
  {
    path: '',
    component: InformazioniBasePage
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InformazioniBasePageRoutingModule {}
