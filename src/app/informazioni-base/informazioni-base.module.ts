import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InformazioniBasePageRoutingModule } from './informazioni-base-routing.module';

import { InformazioniBasePage } from './informazioni-base.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InformazioniBasePageRoutingModule
  ],
  declarations: [InformazioniBasePage]
})
export class InformazioniBasePageModule {}
