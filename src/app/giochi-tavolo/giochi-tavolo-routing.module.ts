import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GiochiTavoloPage } from './giochi-tavolo.page';

const routes: Routes = [
  {
    path: '',
    component: GiochiTavoloPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GiochiTavoloPageRoutingModule {}
