import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GiochiTavoloPageRoutingModule } from './giochi-tavolo-routing.module';

import { GiochiTavoloPage } from './giochi-tavolo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GiochiTavoloPageRoutingModule
  ],
  declarations: [GiochiTavoloPage]
})
export class GiochiTavoloPageModule {}
