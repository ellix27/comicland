import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CosplayPageRoutingModule } from './cosplay-routing.module';

import { CosplayPage } from './cosplay.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CosplayPageRoutingModule
  ],
  declarations: [CosplayPage]
})
export class CosplayPageModule {}
