import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CosplayPage } from './cosplay.page';

const routes: Routes = [
  {
    path: '',
    component: CosplayPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CosplayPageRoutingModule {}
